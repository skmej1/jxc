package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品信息
 */
@Repository
public interface GoodsDao {


    String getMaxCode();


    List<Goods> selectGoodsPage(
            @Param("page") Integer page, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    Long getTotal();

    List<Goods> goodsList(@Param("page") Integer page, @Param("rows") Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    void saveGoods(@Param("vo") Goods goods);

    void updateGoods(@Param("vo") Goods goods, @Param("goodsId") Integer goodsId);

    void delete(@Param("goodsId") Integer goodsId);

    void saveOrUpdateStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    void deleteStock(@Param("goodsId") Integer goodsId);

//    List<Goods> getNoInventoryQuantity(@Param("page") Integer page,
//                                      @Param("rows") Integer rows,
//                                       @Param("nameOrCode") String nameOrCode);

//    Long getTotalNoInventoryQuantity();

}
