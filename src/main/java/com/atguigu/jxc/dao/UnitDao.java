package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: UnitDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 16:25
 * @Version 1.0
 */
@Repository
public interface UnitDao {
    List<Unit> listPage();

}
