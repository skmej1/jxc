package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: CustomerDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 15:24
 * @Version 1.0
 */
@Repository
public interface CustomerDao {
    Long getTotal();

    List<Customer> listPages(@Param("page") Integer page, @Param("rows") Integer rows, @Param("customerName") String customerName);

    void update(@Param("customerId") Integer customerId, @Param("vo") Customer customer);

    void save(@Param("vo") Customer customer);

    void deleteIds(@Param("ids")List<Integer> list);
}
