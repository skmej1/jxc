package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: OverflowListGoodsDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/12 8:38
 * @Version 1.0
 */
@Repository
public interface OverflowListGoodsDao {
    void save(@Param("vo") OverflowListGoods overflowListGoods);

    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);
}
