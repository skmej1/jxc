package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品类别
 */
@Repository
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    void saveType(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId,@Param("goodsTypeState") Integer goodsTypeState);

    void delete(@Param("goodsTypeId") Integer goodsTypeId);

    Integer getFather(@Param("goodsTypeId") Integer goodsTypeId);

    void updateState(@Param("pid") Integer pid, @Param("i") int i);
}
