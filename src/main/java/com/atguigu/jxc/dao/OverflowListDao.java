package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: OverflowListDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/12 8:47
 * @Version 1.0
 */
@Repository
public interface OverflowListDao {
    void save(@Param("vo") OverflowList overflow);

    List<OverflowList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);
}
