package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: SupplierDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 13:59
 * @Version 1.0
 */
@Repository
public interface SupplierDao {
    List<Supplier> listPage(@Param("page") Integer page, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    Long getTotal();

    void update(@Param("supplierId") Integer supplierId, @Param("vo") Supplier supplier);

    void save(@Param("vo") Supplier supplier);

    void deleteIds(@Param("ids") List<Integer> split);
}
