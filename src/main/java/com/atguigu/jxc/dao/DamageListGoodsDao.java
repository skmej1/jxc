package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * ClassName: DamageListGoodsDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 21:09
 * @Version 1.0
 */
@Repository
public interface DamageListGoodsDao {
    void save(@Param("vo") DamageListGoods damageListGoods);

}
