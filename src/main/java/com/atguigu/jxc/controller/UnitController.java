package com.atguigu.jxc.controller;

import com.atguigu.jxc.service.UnitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * ClassName: UnitController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 16:24
 * @Version 1.0
 */
@Api(tags = "商品单位")
@RestController
@RequestMapping("/unit")
public class UnitController {

    @Autowired
    UnitService unitService;

    @ApiOperation("商品单位列表")
    @PostMapping("/list")
    public Map<String,Object> unitList(){
        Map<String,Object> map = unitService.listUnit();
        return map;
    }
}
