package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.Map;

/**
 * ClassName: Damage
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 20:43
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/damageListGoods")
public class DamageController {

    @Autowired
    DamageService damageService;

    @PostMapping("/save")
    public ServiceVO save(
            @RequestParam("damageNumber") String damageNumber,
            @RequestParam("damageListGoodsStr") String damageListGoodsStr,
            DamageList damageList
    ){
        damageService.save(damageNumber,damageListGoodsStr,damageList);
        return new ServiceVO<Customer>(100, "请求成功");
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(
            @RequestParam("sTime") String  sTime,
            @RequestParam("eTime") String  eTime
    ) throws ParseException {
        log.info("damageListGoods/list,报损单查询::起始时间{}::结束时间{}",sTime,eTime);
        return damageService.list(sTime,eTime);
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(
            @RequestParam("damageListId") Integer damageListId
    ){
        return damageService.goodsList(damageListId);
    }
}
