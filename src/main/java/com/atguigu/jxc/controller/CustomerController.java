package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * ClassName: CustomerController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 15:23
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @ApiOperation("客户列表分页（名称模糊查询）")
    @PostMapping("/list")
    public Map<String, Object> customerPage(
            @ApiParam(value = "page", name = "当前页", required = true)
            @RequestParam("page") Integer page,
            @ApiParam(value = "rows", name = "每页显示条数", required = true)
            @RequestParam("rows") Integer rows,
            @ApiParam(value = "customerName", name = "客户名字", required = false)
            @RequestParam(value = "customerName", required = false) String customerName
    ){
        log.info("customer/list,客户列表分页::当前页{}::每页条数{}::用户名字{}",page,rows,customerName);
        return customerService.listPage(page, rows, customerName);
    }

    @ApiOperation("客户添加或修改")
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(
            @ApiParam(value = "customerId", name = "用户id", required = false)
            @RequestParam(value = "customerId",required = false) Integer customerId,
            Customer customer
    ){
        if (customerId!=null){
            customerService.update(customerId,customer);
        }
        if (customerId==null){
            customerService.save(customer);
        }
        return new ServiceVO<Customer>(100, "请求成功");
    }


    @ApiOperation("删除客户（支持批量删除）")
    @PostMapping("/delete")
    public ServiceVO deleteCustomer(
            @RequestParam("ids") String ids
    ){
        customerService.deleteIds(ids);
        return new ServiceVO<Customer>(100, "请求成功");
    }

}
