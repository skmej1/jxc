package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.GoodsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

/**
 * @description 商品信息Controller
 */

@Slf4j
@RestController
@Api(tags = "商品相关")
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @ApiOperation("分页查询商品库存信息")
    @PostMapping("/listInventory")
    public Map<String,Object> getGoodsPage(
            @ApiParam(value = "page",name = "当前页",required = true)
            @RequestParam("page") Integer page,
            @ApiParam(value = "rows",name = "每页显示条数",required = true)
            @RequestParam("rows") Integer rows,
            @ApiParam(value = "codeOrName",name = "商品编码或名称",required = false)
            @RequestParam(value = "codeOrName",required = false) String codeOrName,
            @ApiParam(value = "goodsTypeId",name = "商品类别ID",required = false)
            @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId
    ){
        Map<String, Object> map = goodsService.getGoodsPage(page,rows,codeOrName,goodsTypeId);
        return map;
    }


    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @ApiOperation("分页查询商品信息")
    @PostMapping("/list")
    public Map<String,Object> goodsList(
            @ApiParam(value = "page",name = "当前页",required = true)
            @RequestParam("page") Integer page,
            @ApiParam(value = "rows",name = "每页显示条数",required = true)
            @RequestParam("rows") Integer rows,
            @ApiParam(value = "goodsName",name = "商品名称",required = false)
            @RequestParam(value = "goodsName",required = false) String goodsName,
            @ApiParam(value = "goodsTypeId",name = "商品类别ID",required = false)
            @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId
    ){
        log.info("goods/list,分页查询商品信息::{}::{}::{}::{}",page,rows,goodsName,goodsTypeId);
        return goodsService.getGoodsListWithPage(page,rows,goodsName,goodsTypeId);
    }

    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @ApiOperation("添加或修改商品信息")
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(
            @ApiParam(value = "goodsId",name = "商品编号",required = false)
            @RequestParam(value = "goodsId",required = false) Integer goodsId,
            Goods goods
    ){
        if (goodsId==null){
            goodsService.saveGoods(goods);
            return new ServiceVO<Supplier>(100, "请求成功");
        }
        goodsService.updateGoods(goods,goodsId);
        return new ServiceVO<Supplier>(100, "请求成功");
    }

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */

    @PostMapping("/delete")
    public ServiceVO delete(
            @ApiParam(value = "goodsId",name = "商品编号",required = true)
            @RequestParam(value = "goodsId") Integer goodsId
    ){
        goodsService.deleteGoods(goodsId);
        return new ServiceVO<Supplier>(100, "请求成功");
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @ApiOperation("分页查询无库存商品信息")
    @PostMapping("/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(
            @ApiParam(value = "page",name = "当前页",required = true)
            @RequestParam("page") Integer page,
            @ApiParam(value = "rows",name = "每页显示条数",required = true)
            @RequestParam("rows") Integer rows,
            @ApiParam(value = "nameOrCode",name = "商品名称或编码",required = false)
            @RequestParam(value = "nameOrCode",required = false) String nameOrCode
    ){
        return goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @ApiOperation("分页查询有库存商品信息")
    @PostMapping("/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(
            @ApiParam(value = "page",name = "当前页",required = true)
            @RequestParam("page") Integer page,
            @ApiParam(value = "rows",name = "每页显示条数",required = true)
            @RequestParam("rows") Integer rows,
            @ApiParam(value = "nameOrCode",name = "商品名称或编码",required = false)
            @RequestParam(value = "nameOrCode",required = false) String nameOrCode
    ){
        return goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
    }


    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @PostMapping("/saveStock")
    public ServiceVO saveStock(
            @RequestParam("goodsId") Integer goodsId,
            @RequestParam("inventoryQuantity")Integer inventoryQuantity,
            @RequestParam("purchasingPrice")double purchasingPrice
    ){
        goodsService.saveOrUpdateStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO<Supplier>(100, "请求成功");
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(
            @RequestParam("goodsId") Integer goodsId
    ){
        goodsService.deleteStock(goodsId);
        return new ServiceVO<Supplier>(100, "请求成功");
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    @PostMapping("/listAlarm")
    public Map<String,Object> listAlarm(){
        return goodsService.listAlarm();
    }

}
