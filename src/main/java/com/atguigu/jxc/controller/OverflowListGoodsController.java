package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.Map;

/**
 * ClassName: OverflowListGoodsController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/12 8:35
 * @Version 1.0
 */
@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    OverflowListGoodsService overflowListGoodsService;

    @PostMapping("/save")
    public ServiceVO save(
            @RequestParam("overflowNumber") String overflowNumber,
            @RequestParam("overflowListGoodsStr") String overflowListGoodsStr,
            OverflowList overflowList
    ){
        overflowListGoodsService.saveThis(overflowNumber,overflowListGoodsStr,overflowList);
        return new ServiceVO<Customer>(100, "请求成功");
    }


    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list (
            @RequestParam("sTime") String  sTime,
            @RequestParam("eTime") String  eTime
    ) throws ParseException {
        return overflowListGoodsService.list(sTime,eTime);
    }

    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(
            @RequestParam("overflowListId") Integer overflowListId
    ){
        return overflowListGoodsService.goodsList(overflowListId);
    }

}
