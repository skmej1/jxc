package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.GoodsTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @description 商品类别控制器
 */
@Api(tags = "商品类别")
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 查询所有商品类别
     * @return easyui要求的JSON格式字符串
     */
    @PostMapping("/loadGoodsType")
    @RequiresPermissions(value={"商品管理","进货入库","退货出库","销售出库","客户退货","当前库存查询","商品报损","商品报溢","商品采购统计"},logical = Logical.OR)
    public ArrayList<Object> loadGoodsType() {
        return goodsTypeService.loadGoodsType();
    }

    @ApiOperation("保存类别")
    @PostMapping("/save")
    public ServiceVO saveGoodsType(
            @ApiParam(value = "goodsTypeName",name = "商品类型名称")
            @RequestParam("goodsTypeName") String  goodsTypeName,
            @ApiParam(value = "pId",name = "父类型名称")
            @RequestParam("pId") Integer  pId
    ){
        goodsTypeService.save(goodsTypeName,pId);
        return new ServiceVO<Supplier>(100, "请求成功");
    }

    @ApiOperation("删除类型")
    @PostMapping("/delete")
    public ServiceVO deleteGoodsType(
            @ApiParam(value = "goodsTypeId",name = "商品类型id")
            @RequestParam("goodsTypeId") Integer  goodsTypeId
    ){
        goodsTypeService.delete(goodsTypeId);
        return new ServiceVO<Supplier>(100, "请求成功");
    }
}
