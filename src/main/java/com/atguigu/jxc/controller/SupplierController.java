package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.impl.SupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * ClassName: SupplierController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 13:56
 * @Version 1.0
 */
@Slf4j
@Api(tags = "供应商管理")
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    /**
     * 分页查询供应商
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @ApiOperation("分页查询供应商")
    @PostMapping("/list")
    public Map<String, Object> supplierPage(
            @ApiParam(value = "page", name = "当前页", required = true)
            @RequestParam("page") Integer page,
            @ApiParam(value = "rows", name = "每页显示条数", required = true)
            @RequestParam("rows") Integer rows,
            @ApiParam(value = "supplierName", name = "供应商名字", required = false)
            @RequestParam(value = "supplierName", required = false) String supplierName
    ) {
        log.info("supplier/list,分页查询供应商::当前页{}::每页显示条数{}::供应商名字{}",page,rows,supplierName);
        Map<String, Object> map = supplierService.listPage(page, rows, supplierName);
        return map;
    }

    @ApiOperation("供应商添加或修改")
    @PostMapping("/save")
    public ServiceVO saveSupplier(
            @ApiParam(value = "supplierId", name = "供应商id", required = false)
            @RequestParam(value = "supplierId",required = false) Integer supplierId,
            Supplier supplier
    ) {
        if (supplierId != null) {
            supplierService.updateSupplier(supplierId, supplier);
        }
        if (supplierId == null){
            supplierService.saveSupplier(supplier);
        }
        return new ServiceVO<Supplier>(100, "请求成功");
    }


    @ApiOperation("删除供应商（支持批量删除）")
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(
            @RequestParam("ids") String ids
    ){
        supplierService.deleteIds(ids);
        return new ServiceVO<Supplier>(100, "请求成功");
    }

}
