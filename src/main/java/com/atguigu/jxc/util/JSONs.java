package com.atguigu.jxc.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

/**
 * ClassName: JSONs
 * Package: com.atguigu.gmall.common.util
 * Description:
 *
 * @Author MarxM
 * @Create 2023/8/19 10:05
 * @Version 1.0
 */
@Slf4j
public class JSONs {

    private static final ObjectMapper MAPPER = new ObjectMapper();


    public static <T> T jsonToObj(String json, TypeReference<T> valueTypeRef) {
        T t = null;
        try {
            t = MAPPER.readValue(json, valueTypeRef);
        } catch (JsonProcessingException e) {
            log.error("json转obj失败:" + e);
        }
        return t;
    }

    public static <T> T jsonToObj(String json, Class<T> valueType) {
        T t = null;
        try {
            t = MAPPER.readValue(json, valueType);
        } catch (JsonProcessingException e) {
            log.error("json转obj失败:" + e);
        }
        return t;
    }

    /**
     * 转成json字符串
     *
     * @param obj
     * @return
     */
    public static String toJSON(Object obj) {
        String value = null;
        try {
            value = MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error("obj转json失败:" + e);
        }
        return value;
    }
}
