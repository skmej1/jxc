package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import java.text.ParseException;
import java.util.Map;

/**
 * ClassName: DamageService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 20:46
 * @Version 1.0
 */
public interface DamageService {
    void save(String damageNumber, String damageListGoodsStr, DamageList damageList);

    Map<String, Object> list(String sTime, String eTime) throws ParseException;

    Map<String, Object> goodsList(Integer damageListId);
}
