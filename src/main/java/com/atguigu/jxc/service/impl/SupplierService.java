package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * ClassName: SupplierService
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 13:58
 * @Version 1.0
 */
public interface SupplierService {
    /**
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> listPage(Integer page, Integer rows, String supplierName);

    void updateSupplier(Integer supplierId, Supplier supplier);

    void saveSupplier(Supplier supplier);

    void deleteIds(String ids);
}
