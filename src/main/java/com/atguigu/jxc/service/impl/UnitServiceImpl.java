package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: UnitServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 16:25
 * @Version 1.0
 */
@Service
public class UnitServiceImpl implements UnitService {
    @Autowired
    UnitDao unitDao;
    @Override
    public Map<String, Object> listUnit() {
        HashMap<String, Object> map = new HashMap<>();
        List<Unit> list = unitDao.listPage();
        map.put("rows",list);
        return map;
    }
}
