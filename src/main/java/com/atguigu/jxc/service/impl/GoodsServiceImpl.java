package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;


import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
@Transactional
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> getGoodsPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        List<Goods> list = goodsDao.selectGoodsPage(page-1,rows,codeOrName,goodsTypeId);
        Long total = goodsDao.getTotal();
        Map<String, Object> map = new HashMap<>();
//        List<Goods> records = baseMapper.selectPage(goodsPage, wrapper).getRecords();
        map.put("total",total);
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsListWithPage(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        List<Goods> list = goodsDao.selectGoodsPage(page-1,rows,goodsName,goodsTypeId);
        Long total = goodsDao.getTotal();
        Map<String, Object> map = new HashMap<>();
//        List<Goods> records = baseMapper.selectPage(goodsPage, wrapper).getRecords();
        map.put("total",total);
        map.put("rows",list);
        return map;
    }

    @Override
    public void saveGoods(Goods goods) {
        goodsDao.saveGoods(goods);
    }

    @Override
    public void updateGoods(Goods goods,Integer goodsId) {
        goodsDao.updateGoods(goods,goodsId);
    }

    @Override
    public void deleteGoods(Integer goodsId) {
        goodsDao.delete(goodsId);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        List<Goods> list = goodsDao.selectGoodsPage(page-1, rows, nameOrCode, null).stream()
                .filter(a -> a.getInventoryQuantity() <= 0)
                .collect(Collectors.toList());
        Map<String, Object> map = new HashMap<>();
        map.put("total",list.size());
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        List<Goods> list = goodsDao.selectGoodsPage(page-1, rows, nameOrCode, null).stream()
                .filter(a -> a.getInventoryQuantity() > 0)
                .collect(Collectors.toList());
        Map<String, Object> map = new HashMap<>();
        map.put("total",list.size());
        map.put("rows",list);
        return map;
    }

    @Override
    public void saveOrUpdateStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveOrUpdateStock(goodsId,inventoryQuantity,purchasingPrice);
    }

    @Override
    public void deleteStock(Integer goodsId) {
        goodsDao.deleteStock(goodsId);
    }

    @Override
    public Map<String, Object> listAlarm() {
        List<Goods> list = goodsDao.selectGoodsPage(0, 999, null, null);
        List<Goods> goodsList = list.stream()
                .filter(a -> a.getInventoryQuantity() < a.getMinNum())
                .collect(Collectors.toList());
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",goodsList);
        return map;
    }


}
