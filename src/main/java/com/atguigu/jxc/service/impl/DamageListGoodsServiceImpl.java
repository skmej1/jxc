package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.stereotype.Service;

/**
 * ClassName: DamageListGoodsServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 21:02
 * @Version 1.0
 */
@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
}
