package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.util.DateUtil;
import com.atguigu.jxc.util.JSONs;

import com.fasterxml.jackson.core.type.TypeReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ClassName: DamageServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 20:47
 * @Version 1.0
 */
@Transactional
@Service
public class DamageServiceImpl implements DamageService {
    @Autowired
    DamageDao damageDao;
    @Autowired
    DamageListGoodsDao damageListGoodsDao;

    @Autowired
    UserDao userDao;
    @Override
    public void save(String damageNumber, String damageListGoodsStr, DamageList damageList) {
        DamageList damage = new DamageList();
        damage.setDamageNumber(damageNumber);
        damage.setDamageDate(damageList.getDamageDate());
        damage.setRemarks(damageList.getRemarks());
        damage.setUserId(damageList.getUserId());
        damage.setTrueName(damageList.getTrueName());
        damageDao.save(damage);
        Integer damageListId = damage.getDamageListId();
        String replace = damageListGoodsStr.replace("]", "").replace("[", "");
//        List<DamageListGoods> list = JSONs.jsonToObj(damageListGoodsStr, new TypeReference<List<DamageListGoods>>() {
//        });
        DamageListGoods goods = JSONs.jsonToObj(replace, DamageListGoods.class);
        DamageListGoods damageListGoods = new DamageListGoods();
        damageListGoods.setGoodsId(goods.getGoodsId());
        damageListGoods.setGoodsCode(goods.getGoodsCode());
        damageListGoods.setGoodsName(goods.getGoodsName());
        damageListGoods.setGoodsModel(goods.getGoodsModel());
        damageListGoods.setGoodsUnit(goods.getGoodsUnit());
        damageListGoods.setGoodsNum(goods.getGoodsNum());
        damageListGoods.setPrice(goods.getPrice());
        double doubleValue = new BigDecimal(goods.getGoodsNum()).multiply(new BigDecimal(goods.getPrice())).doubleValue();
        damageListGoods.setTotal(doubleValue);
        damageListGoods.setDamageListId(damageListId);
        damageListGoods.setGoodsTypeId(goods.getGoodsTypeId());
        damageListGoodsDao.save(damageListGoods);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) throws ParseException {
        List<DamageList> list = damageDao.list(sTime,eTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long timeS = sdf.parse(sTime).getTime();
        long timeE = sdf.parse(eTime).getTime();

//        List<DamageList> damageLists = list.stream()
//                .filter(v-> {
//                    long time = 0;
//                    try {
//                        time = sdf.parse(v.getDamageDate()).getTime();
//                    } catch (ParseException e) {
//                        throw new RuntimeException(e);
//                    }
//                    return (time >= timeS && time <= timeE);
//                })
//                .map(a -> {
//                    User user = userDao.getUserById(a.getUserId());
//                    a.setTrueName(user.getTrueName());
//                    return a;
//                }).collect(Collectors.toList());
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        List<DamageListGoods> list = damageDao.goodsList(damageListId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
}
