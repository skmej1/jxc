package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ClassName: CustomerServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 15:24
 * @Version 1.0
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerDao customerDao;
    @Override
    public Map<String, Object> listPage(Integer page, Integer rows, String customerName) {
        List<Customer> list = customerDao.listPages(page-1, rows, customerName);
        Long total = customerDao.getTotal();
        HashMap<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("rows", list);
        return map;
    }

    @Override
    public void update(Integer customerId, Customer customer) {
        customerDao.update(customerId,customer);
    }

    @Override
    public void save(Customer customer) {
        customerDao.save(customer);
    }

    @Override
    public void deleteIds(String ids) {
        String[] split = ids.split(",");
        List<Integer> list = Arrays.stream(split).map(Integer::new)
                .collect(Collectors.toList());
        customerDao.deleteIds(list);
    }
}
