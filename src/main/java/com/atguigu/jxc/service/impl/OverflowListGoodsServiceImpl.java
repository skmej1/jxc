package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.atguigu.jxc.util.JSONs;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ClassName: OverflowListGoodsServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/12 8:36
 * @Version 1.0
 */
@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {
    @Autowired
    OverflowListGoodsDao overflowListGoodsDao;
    @Autowired
    OverflowListDao overflowListDao;

    @Autowired
    UserDao userDao;

    @Override
    public void saveThis(String overflowNumber, String overflowListGoodsStr, OverflowList overflowList) {
        OverflowList overflow = new OverflowList();
        overflow.setOverflowNumber(overflowNumber);
        overflow.setOverflowDate(overflowList.getOverflowDate());
        overflow.setRemarks(overflowList.getRemarks());
        overflow.setUserId(overflowList.getUserId());
        overflow.setTrueName(overflowList.getTrueName());
        overflowListDao.save(overflow);
        Integer overflowListId = overflow.getOverflowListId();
        List<OverflowListGoods> list = JSONs.jsonToObj(overflowListGoodsStr, new TypeReference<List<OverflowListGoods>>() {
        });
        OverflowListGoods goods = list.get(0);
        OverflowListGoods overflowListGoods = new OverflowListGoods();
        overflowListGoods.setGoodsId(goods.getGoodsId());
        overflowListGoods.setGoodsCode(goods.getGoodsCode());
        overflowListGoods.setGoodsName(goods.getGoodsName());
        overflowListGoods.setGoodsModel(goods.getGoodsModel());
        overflowListGoods.setGoodsUnit(goods.getGoodsUnit());
        overflowListGoods.setGoodsNum(goods.getGoodsNum());
        overflowListGoods.setPrice(goods.getPrice());
        double value = new BigDecimal(goods.getGoodsNum()).multiply(new BigDecimal(goods.getGoodsNum())).doubleValue();
        overflowListGoods.setTotal(value);
        overflowListGoods.setOverflowListId(overflowListId);
        overflowListGoods.setGoodsTypeId(goods.getGoodsTypeId());
        overflowListGoodsDao.save(overflowListGoods);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long timeS = sdf.parse(sTime).getTime();
        long timeE = sdf.parse(eTime).getTime();
        List<OverflowList> list = overflowListDao.list(sTime,eTime);
        List<OverflowList> overflowLists = list.stream()
                .filter(b-> {
                    long time = 0;
                    try {
                        time = sdf.parse(b.getOverflowDate()).getTime();
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }
                    return (time >= timeS && time <= timeE);
                })
                .map(a -> {
                    User user = userDao.getUserById(a.getUserId());
                    a.setTrueName(user.getTrueName());
                    return a;
                }).collect(Collectors.toList());
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",overflowLists);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        List<OverflowListGoods> list = overflowListGoodsDao.goodsList(overflowListId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
}
