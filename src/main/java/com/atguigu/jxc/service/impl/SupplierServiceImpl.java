package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ClassName: SupplierServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 13:58
 * @Version 1.0
 */
@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    SupplierDao supplierDao;

    @Override
    public Map<String, Object> listPage(Integer page, Integer rows, String supplierName) {
        List<Supplier> list = supplierDao.listPage(page-1, rows, supplierName);
        Long total = supplierDao.getTotal();
        HashMap<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("rows", list);
        return map;
    }

    @Override
    public void updateSupplier(Integer supplierId, Supplier supplier) {
        supplierDao.update(supplierId,supplier);
    }

    @Override
    public void saveSupplier(Supplier supplier) {
        supplierDao.save(supplier);
    }

    @Override
    public void deleteIds(String ids) {
        String[] split = ids.split(",");
        List<Integer> list = Arrays.stream(split).map(Integer::new)
                .collect(Collectors.toList());
        supplierDao.deleteIds(list);
    }

}
