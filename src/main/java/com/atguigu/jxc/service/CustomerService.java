package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * ClassName: CustomerService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 15:23
 * @Version 1.0
 */
public interface CustomerService {
    Map<String, Object> listPage(Integer page, Integer rows, String customerName);

    void update(Integer customerId, Customer customer);

    void save(Customer customer);

    void deleteIds(String ids);
}
