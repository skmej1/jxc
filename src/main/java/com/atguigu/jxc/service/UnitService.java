package com.atguigu.jxc.service;

import java.util.Map;

/**
 * ClassName: UnitService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/11 16:25
 * @Version 1.0
 */

public interface UnitService {
    Map<String, Object> listUnit();
}
