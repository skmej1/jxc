package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import java.text.ParseException;
import java.util.Map;

/**
 * ClassName: OverflowListGoods
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author MarxM
 * @Create 2023/9/12 8:35
 * @Version 1.0
 */
public interface OverflowListGoodsService {
    void saveThis(String overflowNumber, String overflowListGoodsStr, OverflowList overflowList);

    Map<String, Object> list(String sTime, String eTime) throws ParseException;

    Map<String, Object> goodsList(Integer overflowListId);

}
