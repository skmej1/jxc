package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    Map<String, Object> getGoodsPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    Map<String, Object> getGoodsListWithPage(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void saveGoods(Goods goods);

    void updateGoods(Goods goods,Integer goodsId);

    void deleteGoods(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveOrUpdateStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    void deleteStock(Integer goodsId);

    Map<String, Object> listAlarm();
}
